import View from './view';
import FighterView from './fighterView';
import { fighterService } from './services/fightersService';
import {fighteRRR} from './fighter';
class FightersView extends View {
  constructor(fighters) {
    super();
    this.handleClick = this.handleFighterClick.bind(this);
    this.createFighters(fighters);
  }
  fightersDetailsMap = new Map();

  createFighters(fighters) {
    const fighterElements = fighters.map(fighter => {
      const fighterView = new FighterView(fighter, this.handleClick);
      return fighterView.element;
    });

    this.element = this.createElement({ tagName: 'div', className: 'fighters' });
    this.element.append(...fighterElements);
  }
  // set info into fightersDetailsMap
   setInfo(fighters) {
    fighters.map(async fighterItem => {
      const fighter = await fighterService.getFighterDetails(fighterItem._id);
      const {_id } = fighter;
        this.fightersDetailsMap.set(`${_id}`, fighter);
    });
  }
// show modal then clicked on get info button
  showModal(id) {
   const {health, attack, defense, name } = this.fightersDetailsMap.get(id);
   const modal = `
    <div class="modal-close">
        <div class="modal active">
        <svg class="modal__cross " xmlns="http://www.w3.org/2000/svg" viewBox="0 0 24 24"><path d="M23.954 21.03l-9.184-9.095 9.092-9.174-2.832-2.807-9.09 9.179-9.176-9.088-2.81 2.81 9.186 9.105-9.095 9.184 2.81 2.81 9.112-9.192 9.18 9.1z"/></svg>
        <p class="modal__title">
        <span class="name">Name: ${name}</span>
        <span class="name">Health: ${health}</span>
        <span class="name">Attack: ${attack}</span>
        <span class="name">Defense: ${defense}</span>
        </p>
     </div>
     <div class="overlay active"></div>
     </div>
     `;
    document.body.insertAdjacentHTML('afterbegin', modal);
  }
  // show button then 2 fighters choosed
  showFightButton(){
    const nameElement = this.createElement({ tagName: 'button', className: 'fight' });
    nameElement.innerText = 'START FIGHT';
    document.body.append(nameElement)
  }
  // hide button if 2 fighters not choosed
  hideFightButton(){
    const element = document.querySelector('.fight')
    document.body.removeChild(element);
  }
// hide modal then clicked on X or outside 
 hideModal(){
   let closeButton = document.querySelector('.modal__cross');
   closeButton.addEventListener('click',e =>{
   let element = document.querySelector('.modal-close');
   element.parentElement.removeChild(element);
   });

   let overlay = document.querySelector('.overlay');
   overlay.addEventListener('click',e =>{
    let element = document.querySelector('.modal-close');
    element.parentElement.removeChild(element);
    });
 }
 startFight(){
   const [fighterIdOne, fighterIdTwo] = this.fightersDetailsMap.get('fighters');
   const fighterOne = this.fightersDetailsMap.get(fighterIdOne);
   const fighterTwo = this.fightersDetailsMap.get(fighterIdTwo);
  
//  remove button,checkbox  then game is started
   const fighterReady = document.querySelectorAll(`input:checked`);
   fighterReady.forEach(elem =>{
     elem.previousSibling.remove();
    elem.remove();
   })

  //  disable All checkboxes then game is started
      fighteRRR.fight(fighterOne, fighterTwo, this.fightersDetailsMap);
    document.querySelectorAll(`input[type='checkbox']`).forEach(elem =>{
       elem.disabled = true;
      })
 };
 chooseFighter(){
  const fighters = [];
  const checkedBoxes = document.querySelectorAll('input:checked');
  if(checkedBoxes.length >= 2){
    document.querySelectorAll('input:not(:checked)').forEach(elem=>{
      elem.disabled = true;
    });
  } else {
    document.querySelectorAll('input:not(:checked)').forEach(elem=>{
      elem.disabled = false;
    });
  }
  if(checkedBoxes.length >= 2 ){
    checkedBoxes.forEach(element => {
      let id = element.parentElement.getAttribute('data-id')
      fighters.push(id);
    });
    this.fightersDetailsMap.set('fighters', fighters)
    // show button then 2 fighters choosed
    this.showFightButton()
    document.querySelector('.fight').addEventListener('click', e => {
      this.startFight()
      // after start fight change properties of button
      document.querySelector('.fight').innerText = 'START NEW GAME';
      document.querySelector('.fight').classList.add('new-game');
      // reload page then clicked on START NEW GAME button
      if(e.target.classList.contains('new-game')){
       e.target.addEventListener('click', () => location.reload())
      }
    });
  } 
  // hide button if 2 fighters not choosed
  else if(checkedBoxes.length < 2 && document.querySelector('.fight')){
    this.hideFightButton();
  }
}
   handleFighterClick(event, fighter) { 
    let element = event.target;
    const id = fighter._id;
      if(element.classList.contains("fighter-info")){
      this.showModal(id);
      this.hideModal();
    }
    if (element.classList.contains("fighter-checkbox")) {
          this.chooseFighter();
      }
  }
}

export default FightersView;